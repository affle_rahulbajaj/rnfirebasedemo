/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  Colors
} from 'react-native/Libraries/NewAppScreen';
import messaging from '@react-native-firebase/messaging';
import RnVizuryLogger from 'react-native-rn-vizury-logger';

export default class App extends React.Component {

  componentDidMount() {
    this.requestUserPermission()
  }

  requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
    let fcmToken = await messaging().getToken();
    console.log('fcmToken:', fcmToken);

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log('Notification received in background state:', remoteMessage.notification);
    });

    messaging().onMessage((response) => {
      // console.log('Notification received in foreground state', response);
      this.isPushFromVizuryHandler(response)
    });

    messaging().getInitialNotification().then(remoteMessage => {
      if (remoteMessage) {
        console.log('Notification received in killed state', remoteMessage.notification);
      }
    });

  }

  isPushFromVizuryHandler = async (message) => {
    if (Platform.OS === 'android') {
      let sendingMap = {};
      sendingMap["attributes"] = message.data;
      const isPushFromVizury = await RnVizuryLogger.isPushFromVizury(sendingMap);
      if (__DEV__) console.log('isPushFromVizury', isPushFromVizury)
      if (isPushFromVizury.value) {
        RnVizuryLogger.handleNotificationReceived(sendingMap);
      } else {
        // your own logic goes here
      }
    }
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <Header />
            {global.HermesInternal == null ? null : (
              <View style={styles.engine}>
                <Text style={styles.footer}>Engine: Hermes</Text>
              </View>
            )}
            <View style={styles.body}>
              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>Step One</Text>
                <Text style={styles.sectionDescription}>
                  Edit <Text style={styles.highlight}>App.js</Text> to change this
                  screen and then come back to see your edits.
                </Text>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});