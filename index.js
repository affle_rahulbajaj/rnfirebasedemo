/**
 * @format
 */

import { AppRegistry, Platform } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import messaging from '@react-native-firebase/messaging';
import RnVizuryLogger from 'react-native-rn-vizury-logger';

messaging().setBackgroundMessageHandler(async message => {
    if (Platform.OS === 'android') {
        let sendingMap = {};
        sendingMap["attributes"] = message.data;
        const isPushFromVizury = await RnVizuryLogger.isPushFromVizury(sendingMap);
        console.log('isPushFromVizury', isPushFromVizury)
        if (isPushFromVizury.value) {
            RnVizuryLogger.handleNotificationReceived(sendingMap);
        } else {
            // your own logic goes here
        }
    }
});

AppRegistry.registerComponent(appName, () => App);